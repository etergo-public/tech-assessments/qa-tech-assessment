package com.etergo.techassessment;

import com.etergo.techassessment.MainActivity.UiModel;

class ZoomViewModel implements IZoomViewModel {

    private IZoomLevelRepository repository;

    public ZoomViewModel(IZoomLevelRepository repository) {
        this.repository = repository;
    }

    @Override
    public UiModel onStart() {
        return updateModel(repository.getZoomValue());
    }

    @Override
    public UiModel onZoomIn() {
        return updateModel(repository.zoomIn());
    }

    @Override
    public UiModel onZoomOut() {
        return updateModel(repository.zoomOut());
    }

    private UiModel updateModel(int zoomValue) {
        return new UiModel(zoomValue, zoomValue < 200, zoomValue > 25);
    }
}
