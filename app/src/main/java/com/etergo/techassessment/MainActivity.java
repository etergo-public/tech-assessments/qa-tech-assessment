package com.etergo.techassessment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private Button btnZoomIn, btnZoomOut;
    private TextView textViewZoomValue;
    private IZoomViewModel zoomViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnZoomIn = findViewById(R.id.btnZoomIn);
        btnZoomOut = findViewById(R.id.btnZoomOut);
        textViewZoomValue = findViewById(R.id.textViewZoomValue);

        btnZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleZoomIn();
            }
        });
        btnZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleZoomOut();
            }
        });

        injectDependencies();

        updateUi(zoomViewModel.onStart());
    }

    private void injectDependencies() {
        final IInjector injector = (IInjector)getApplication();
        zoomViewModel = injector.getZoomViewModel();
    }


    private void updateUi(UiModel uiModel) {
        btnZoomIn.setEnabled(uiModel.isZoomInEnabled);
        btnZoomOut.setEnabled(uiModel.isZoomOutEnabled);
        textViewZoomValue.setText(getString(R.string.text_zoom_value, uiModel.zoomValue));
    }

    private void handleZoomIn() {
        updateUi(zoomViewModel.onZoomIn());
    }

    private void handleZoomOut() {
        updateUi(zoomViewModel.onZoomOut());
    }

    static class UiModel {
        private final int zoomValue;
        private final boolean isZoomInEnabled;
        private final boolean isZoomOutEnabled;

        UiModel(int zoomValue, boolean isZoomInEnabled, boolean isZoomOutEnabled) {
            this.zoomValue = zoomValue;
            this.isZoomInEnabled = isZoomInEnabled;
            this.isZoomOutEnabled = isZoomOutEnabled;
        }
    }
}
