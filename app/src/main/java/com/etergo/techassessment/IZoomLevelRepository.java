package com.etergo.techassessment;

public interface IZoomLevelRepository {
    int getZoomValue();
    int zoomIn();
    int zoomOut();

}
