package com.etergo.techassessment;

import android.app.Application;

public class MainApplication extends Application implements IInjector {

    private IZoomViewModel zoomViewModel;
    private IZoomLevelRepository zoomLevelRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        zoomLevelRepository = new ZoomLevelRepository(100);
        zoomViewModel = new ZoomViewModel(zoomLevelRepository);
    }

    @Override
    public IZoomViewModel getZoomViewModel() {
        return this.zoomViewModel;
    }

    @Override
    public IZoomLevelRepository getZoomLevelRepository() {
        return this.zoomLevelRepository;
    }
}
