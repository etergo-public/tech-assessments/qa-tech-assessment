package com.etergo.techassessment;

class ZoomLevelRepository implements IZoomLevelRepository {

    private int zoomValue;

    public ZoomLevelRepository(int initValue) {
        this.zoomValue = initValue;
    }

    @Override
    public int getZoomValue() {
        return zoomValue;
    }

    @Override
    public int zoomIn() {
        if (zoomValue < 200) zoomValue += 25;
        return zoomValue;
    }

    @Override
    public int zoomOut() {
        if (zoomValue > 25) zoomValue -= 25;
        return zoomValue;
    }
}
