package com.etergo.techassessment;

public interface IInjector {
    IZoomLevelRepository getZoomLevelRepository();
    IZoomViewModel getZoomViewModel();
}
