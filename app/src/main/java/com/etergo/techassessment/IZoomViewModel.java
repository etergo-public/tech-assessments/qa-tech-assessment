package com.etergo.techassessment;

import com.etergo.techassessment.MainActivity.UiModel;

public interface IZoomViewModel {
    UiModel onStart();
    UiModel onZoomIn();
    UiModel onZoomOut();
}
