## Specifications

1. The screen has the following UI components from top to bottom
 - Header text
 - Zoom in button
 - A text view that displays current zoom value
 - Zoom out button
2. The starting zoom value (when the app is started) is 100% 
3. Clicking on the Zoom in button increases the zoom percentage by 25, up to a maximum of 200. 
  - Once the zoom value reaches 200%, this button is disabled.
  - It is re-enabled once the Zoom Out button is clicked.
4. Clicking the Zoom out button decreases the zoom percentage by 25, up to a minimum of 25.
  - Once the zoom value reaches 25%, this button is disabled.
  - It is re-enabled once the Zoom In button is clicked.

The app has been tested on 7-inch WSVGA (Tablet) emulator with screen dimensions of 1024x600
  
## Task

1. Explain how you would go about testing this application. Feel free to write up in detail in a markdown document/text file/any other medium you prefer.
2. Write automated functional tests for this application. You are free to choose the framework that you are comfortable with (Appium/UiAutomator/Cucumber/other).

**Note:** Do not spend more than one day on this task.

### Evaluation criteria

1. General approach to testing
2. Completeness of test cases
3. **Bonus:** Having unit tests using JUnit/Espresso/Robolectric

## Architecture

This application has 3 layers
1. View (`MainActivity`). This layer is responsible for showing the elements on screen and setting their properties, and handling user input events. There is no logic in this layer.
2. ViewModel (represented by the interface `IZoomViewModel`, implemented with `ZoomViewModel`). This layer has all the business logic. Every operation on this layer returns a `UiModel` object which is used by the View layer to set the properties to the UI elements. 
3. Repository (represented by the interface `IZoomLevelRespository`, implemented with `ZoomLevelRespository`). This layer is the data source.

The `IInjector` interface is useful for injecting your own implementations of the ViewModel or repository for unit tests. See `MainApplication` for details of how this is used.

## Hints

Pay attention to the device orientation, and to multiple languages that the app has been translated to.